module poc-orders

go 1.16

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/gofiber/fiber/v2 v2.14.0
	github.com/joho/godotenv v1.3.0
	github.com/thoas/go-funk v0.9.0
	go.uber.org/zap v1.18.1
	gorm.io/driver/mysql v1.1.1
	gorm.io/gorm v1.21.11
)
