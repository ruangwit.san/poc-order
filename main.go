package main

import (
	"poc-orders/src/environment"
	"poc-orders/src/routers"
)

func main() {

	environment.Init()

	r := routers.Setup()
	r.Listen(":8999")

}
