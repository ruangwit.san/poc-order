package controllers

import "github.com/gofiber/fiber/v2"

type restaurantControllerInterface interface {
	FindOrders(*fiber.Ctx) error
}
