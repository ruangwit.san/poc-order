package controllers

import (
	"fmt"
	customContext "poc-orders/src/core/context"
	"poc-orders/src/entities"
	"poc-orders/src/repositories"
	"poc-orders/src/services"
	"strconv"

	"github.com/gofiber/fiber/v2"
)

type OrderControllerInterface interface {
	FindAll(*fiber.Ctx) error
	Create(c *fiber.Ctx) error
	FindById(c *fiber.Ctx) error
	UpdateStatus(c *fiber.Ctx) error
	FindRestaurantOrder(c *fiber.Ctx) error
}

type order struct {
	orderService services.OrderServiceInterface
}

func NewOrderController(repo repositories.BaseRepo) *order {
	return &order{
		orderService: services.NewOrderService(repo),
	}
}

func (or *order) FindAll(c *fiber.Ctx) error {
	ctx := c.Locals("cc").(*customContext.Context)
	res := ctx.Response()

	models, err := or.orderService.FindAll()
	if err != nil {
		return res.ErrorMessage(err.Error()).Return()
	}

	return res.Data(models).Return()
}

func (or *order) Create(c *fiber.Ctx) error {
	ctx := c.Locals("cc").(*customContext.Context)
	res := ctx.Response()
	payload := entities.Orders{}
	c.BodyParser(&payload)
	if _, err := or.orderService.Create(payload); err != nil {
		return res.ErrorMessage(err.Error()).Return()
	}
	return res.Data(payload).Return()
}

func (or *order) FindById(c *fiber.Ctx) error {
	ctx := c.Locals("cc").(*customContext.Context)
	res := ctx.Response()

	paramsId := c.Params("id")
	id, _ := strconv.Atoi(paramsId)

	orderModel, err := or.orderService.FindById(uint(id))

	if err != nil {
		return res.ErrorMessage(err.Error()).Return()
	}
	return res.Data(orderModel).Return()
}

func (or *order) UpdateStatus(c *fiber.Ctx) error {

	ctx := c.Locals("cc").(*customContext.Context)
	res := ctx.Response()
	payload := entities.Orders{}
	c.BodyParser(&payload)
	id := payload.ID
	status := payload.Status

	orderModel, err := or.orderService.UpdateStatus(id, status)

	if err != nil {
		return res.ErrorMessage(err.Error()).Return()
	}
	return res.Data(orderModel).Return()

}

func (or *order) FindRestaurantOrder(c *fiber.Ctx) error {
	fmt.Println("FindRestaurantOrder")
	ctx := c.Locals("cc").(*customContext.Context)
	res := ctx.Response()

	id := c.Params("id")
	resId, _ := strconv.Atoi(id)

	orderList, err := or.orderService.FindRestaurantOrder(uint(resId))

	if err != nil {
		return res.ErrorMessage(err.Error()).Return()
	}
	return res.Data(orderList).Return()

}
