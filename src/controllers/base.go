package controllers

import (
	"poc-orders/src/repositories"

	"gorm.io/gorm"
)

type controller struct {
	OrderController      OrderControllerInterface
	restaurantController restaurantControllerInterface
}

func NewControllers(db *gorm.DB) *controller {
	repo := repositories.BaseRepo{
		GormDB: db,
	}
	return &controller{
		OrderController: NewOrderController(repo),
	}

}
