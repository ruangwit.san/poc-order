package services

import "poc-orders/src/repositories"

type restaurant struct {
	repo           *repositories.BaseRepo
	restaurantRepo repositories.RestaurantRepositoryInterface
}

type RestaurantServiceInterface interface {
}

func NewRestaurantService(repo repositories.BaseRepo) *restaurant {
	return &restaurant{
		repo:           &repo,
		restaurantRepo: repositories.RestaurantNew(repo),
	}
}
