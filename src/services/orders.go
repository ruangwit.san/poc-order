package services

import (
	"poc-orders/src/entities"
	"poc-orders/src/repositories"
)

type order struct {
	repo      *repositories.BaseRepo
	orderRepo repositories.OrdersRepositryInterface
}

type OrderServiceInterface interface {
	FindAll() ([]*entities.Orders, error)
	FindById(uint) (*entities.Orders, error)
	UpdateStatus(uint, string) (*entities.Orders, error)
	Create(entities.Orders) (*entities.Orders, error)
	FindRestaurantOrder(uint) ([]*entities.Orders, error)
}

func NewOrderService(repo repositories.BaseRepo) *order {
	return &order{
		repo:      &repo,
		orderRepo: repositories.OrderNew(repo),
	}
}

func (or *order) FindAll() ([]*entities.Orders, error) {
	// dOrder := make([]*entities.Orders, 0)
	orders, err := or.orderRepo.Find()
	if err != nil {
		return nil, err
	}

	// for _, v := range orders {
	// 	dOrder = append(dOrder, v.GetOutput("display"))
	// }

	return orders, err

}

func (or *order) FindById(id uint) (*entities.Orders, error) {

	order, err := or.orderRepo.FindById(id)
	if err != nil {
		return nil, err
	}

	return order, nil
}

func (or *order) UpdateStatus(id uint, status string) (*entities.Orders, error) {
	eOrder, err := or.orderRepo.FindById(id)

	if err != nil {
		return nil, err
	}

	eOrder.Status = status

	if err := or.orderRepo.Update(eOrder); err != nil {
		return nil, err
	}
	return eOrder, nil

}

func (or *order) Create(payload entities.Orders) (*entities.Orders, error) {
	if err := or.orderRepo.Create(&payload); err != nil {
		return nil, err
	}
	return &payload, nil
}

func (or *order) FindRestaurantOrder(resId uint) ([]*entities.Orders, error) {

	orderList, err := or.orderRepo.FindRestaurantOrder(resId)

	if err != nil {
		return nil, err
	}
	return orderList, nil
}
