package pagination

import (
	"poc-orders/src/utils"

	"github.com/gofiber/fiber/v2"
)

const (
	defaultPageSize int = 10
	maxPageSize     int = 100
)

type Paginate struct {
	Page       int `json:"page"`
	PerPage    int `json:"per_page"`
	PageCount  int `json:"page_count"`
	TotalCount int `json:"total_count"`
}

func New(c *fiber.Ctx) *Paginate {
	page := utils.StringToInt(c.Query("page"), 0)
	perPage := utils.StringToInt(c.Query("per_page"), defaultPageSize)

	if perPage <= 0 {
		perPage = defaultPageSize
	}

	if perPage > maxPageSize {
		perPage = maxPageSize
	}

	if page < 1 {
		page = 1
	}

	return &Paginate{
		Page:    page,
		PerPage: perPage,
	}
}

// Offset returns the OFFSET value
func (p *Paginate) Offset() int {
	return (p.Page - 1) * p.PerPage
}

// Limit returns the LIMIT value
func (p *Paginate) Limit() int {
	return p.PerPage
}

// SetTotal returns the page total value
func (p *Paginate) SetTotal(t int64) *Paginate {
	if t == 0 {
		return p
	}

	total := int(t)
	pageCount := (total + p.PerPage - 1) / p.PerPage
	if p.Page > pageCount {
		p.Page = pageCount
	}

	p.TotalCount = total
	p.PageCount = pageCount
	return p
}
