package responses

import (
	"poc-orders/src/constant"
	"poc-orders/src/utils/pagination"
	"regexp"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
)

/**
 * Example
 * defalut status = 200
 *
 * Output(ctx).Status(200).Pagination(10).Data(data).Do()
 */
type (
	APIStatus struct {
		Code        int       `json:"code,omitempty"`
		CurrentTime time.Time `json:"current_time,omitempty"`
	}
	APIError struct {
		Validate map[string]interface{} `json:"validate,omitempty"`
	}
	APIResult struct {
		Status  APIStatus            `json:"status,omitempty"`
		Meta    *pagination.Paginate `json:"meta,omitempty"`
		Data    interface{}          `json:"data,omitempty"`
		Message string               `json:"message,omitempty"`
		Errors  *APIError            `json:"errors,omitempty"`
	}

	ResCall struct {
		ctx      *fiber.Ctx
		status   int
		code     int
		message  string
		meta     *pagination.Paginate
		validate map[string]interface{}
		data     interface{}
	}
)

func New(c *fiber.Ctx) *ResCall {
	statusOK := fiber.StatusOK
	r := &ResCall{
		ctx:    c,
		status: statusOK,
		code:   statusOK,
	}
	return r
}

func (r *ResCall) Status(s int) *ResCall {
	r.status = s
	r.code = s
	return r
}

func (r *ResCall) Data(data interface{}) *ResCall {
	r.data = data
	return r
}

func (r *ResCall) Validate(err error) *ResCall {
	validate := make(map[string]interface{})
	var rgx = regexp.MustCompile(`\((.*?)\)`)
	var split = strings.Split(err.Error(), ",")

	if len(split) > 0 {
		for _, v := range split {
			rs := rgx.FindStringSubmatch(v)
			if len(rs) != 2 {
				continue
			}
			validate[rs[1]] = strings.Replace(v, rs[0], "", -1)
		}
	}

	r.code = fiber.StatusBadRequest
	r.validate = validate
	r.message = constant.ErrInvalidValidation
	return r
}

func (r *ResCall) Pagination(p *pagination.Paginate) *ResCall {
	r.meta = p
	return r
}

func (r *ResCall) ErrorMessage(message string) *ResCall {
	c := fiber.StatusBadRequest
	r.code = c
	r.status = c
	r.message = message
	return r
}

func (r *ResCall) Message(message string) *ResCall {
	r.message = message
	return r
}

func (r *ResCall) DataNotFound() *ResCall {
	r.code = fiber.StatusNotFound
	r.message = constant.ErrDataNotFound
	return r
}

func (r *ResCall) Unauthorized() *ResCall {
	c := fiber.StatusUnauthorized
	r.code = c
	r.status = c
	r.message = constant.ErrUnauthorized
	return r
}

func (r *ResCall) TokenExpire() *ResCall {
	c := fiber.StatusUnauthorized
	r.code = c
	r.status = c
	r.message = constant.ErrExpireToken
	return r
}

func (r *ResCall) TokenInvalid() *ResCall {
	c := fiber.StatusUnauthorized
	r.code = c
	r.status = c
	r.message = constant.ErrInvalidToken
	return r
}

func (r *ResCall) InternalServerError() *ResCall {
	c := fiber.StatusInternalServerError
	r.code = c
	r.status = c
	r.message = constant.ErrInternalServerError
	return r
}

func (r *ResCall) Return() error {
	time, _ := time.Parse(time.RFC3339, time.Now().Format(time.RFC3339))

	res := &APIResult{
		Status: APIStatus{
			Code:        r.code,
			CurrentTime: time.UTC(),
		},
		Meta:    r.meta,
		Data:    r.data,
		Message: r.message,
	}

	if len(r.validate) > 0 {
		res.Errors = &APIError{
			Validate: r.validate,
		}
	}

	return r.ctx.Status(r.status).JSON(res)
}
