package utils

import (
	"os"
	"strconv"
)

func Env(name, defaultValue string) string {
	if env := os.Getenv(name); env != "" {
		return env
	}
	return defaultValue
}

func StringToInt(value string, defaultValue int) int {
	if result, err := strconv.Atoi(value); err == nil {
		return result
	}
	return defaultValue
}

// func StringArrayToIntArray(s string, sep string) (i []int) {
// 	for _, v := range strings.Split(s, sep) {
// 		i = append(i, StringToInt(v, 0))
// 	}

// 	i = funk.Uniq(i).([]int)

// 	return
// }

// func RandString(n int) string {
// 	const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
// 	b := make([]byte, n)
// 	for i := range b {
// 		b[i] = letterBytes[mathRand.Intn(len(letterBytes))]
// 	}
// 	return string(b)
// }

// func Request() *gorequest.SuperAgent {
// 	// Doc https://github.com/parnurzeal/gorequest/blob/develop/gorequest.go
// 	return gorequest.New().
// 		Timeout(30 * time.Second).
// 		TLSClientConfig(&tls.Config{InsecureSkipVerify: true})
// }

// func CamelCaseToUnderscore(str string) string {
// 	var output []rune
// 	var segment []rune
// 	for _, r := range str {
// 		if !unicode.IsLower(r) && string(r) != "_" && !unicode.IsNumber(r) {
// 			output = addSegment(output, segment)
// 			segment = nil
// 		}
// 		segment = append(segment, unicode.ToLower(r))
// 	}
// 	output = addSegment(output, segment)
// 	return string(output)
// }

// func addSegment(inrune, segment []rune) []rune {
// 	if len(segment) == 0 {
// 		return inrune
// 	}
// 	if len(inrune) != 0 {
// 		inrune = append(inrune, '_')
// 	}
// 	inrune = append(inrune, segment...)
// 	return inrune
// }
