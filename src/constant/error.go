package constant

const (
	ErrUserSuspended     = "บัญชีนี้ถูกระงับการใช้งานชั่วคราว"
	ErrUserPassIncorrect = "ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง"
	ErrUserVerifyEmail   = "กรุณายืนยันตัวตนอีเมลของคุณ"

	ErrInvalidToken         = "เกิดข้อผิดพลาด token ไม่ถูกต้อง"
	ErrInvalidTokenSocial   = "เกิดข้อผิดพลาด token social ไม่ถูกต้อง"
	ErrInvalidTokenClientId = "Invalid token client id."
	ErrExpireToken          = "Token หมดอายุ"
	ErrExpireTokenSocial    = "Token social หมดอายุ"
	ErrSocialNotFoundUser   = "Social id not found with user."

	ErrOtpRefIncorrect = "รหัส OTP ไม่ถูกต้อง"
	ErrOtpLimit        = "คุณทำรายการขอ OTP เกินที่กำหนด"
	ErrOtpExpire       = "รหัส OTP หมดอายุ"
	ErrOtpCanNotSend   = "ไม่สามารถส่ง Otp ได้"

	ErrDuplicateMobileNumber      = "หมายเลขโทรศัพท์นี้ถูกใช้งานแล้ว"
	ErrDuplicateSocial            = "บัญชี social ถูกใช้งานแล้ว"
	ErrDuplicateDataConnectSocial = "ไม่สามารถเชื่อมต่อได้ เนื่องจากบัญชีนี้มีการเชื่อมต่อไปแล้ว"

	ErrInvalidData       = "ข้อมูลบ้างอย่างไม่ถูกต้อง"
	ErrInvalidValidation = "ข้อมูลไม่ถูกต้อง"

	ErrDataNotFound        = "Data not found."
	ErrUnauthorized        = "Unauthorized"
	ErrInternalServerError = "Internal server error"
)
