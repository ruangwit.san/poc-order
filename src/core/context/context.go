package context

import (
	"poc-orders/src/utils/responses"

	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"
)

type Context struct {
	*fiber.Ctx
}

func New(ctx *fiber.Ctx) *Context {
	return &Context{ctx}
}

func (c *Context) Response() *responses.ResCall {
	return responses.New(c.Ctx)
}

func (c *Context) Logger() *zap.Logger {
	logger, _ := zap.NewProduction()
	defer logger.Sync()

	return logger
}
