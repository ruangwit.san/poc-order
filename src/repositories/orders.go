package repositories

import (
	"poc-orders/src/constant"
	"poc-orders/src/entities"
)

type OrdersRepositryInterface interface {
	Find() ([]*entities.Orders, error)
	FindById(uint) (*entities.Orders, error)
	Create(*entities.Orders) error
	Update(*entities.Orders) error
	FindRestaurantOrder(uint) ([]*entities.Orders, error)
}

type order struct {
	baseRepo *BaseRepo
}

func OrderNew(repo BaseRepo) OrdersRepositryInterface {
	return &order{
		baseRepo: &repo,
	}

}

func (o order) Create(order *entities.Orders) (err error) {
	err = o.baseRepo.GormDB.Omit("User").Create(&order).Debug().Error
	return
}

func (o order) Update(order *entities.Orders) (err error) {
	err = o.baseRepo.GormDB.Save(&order).Debug().Error
	return
}

func (o order) Find() (orders []*entities.Orders, err error) {
	if err = o.baseRepo.GormDB.Preload("OrderItems").Preload("Restaurant").Preload("User").Find(&orders).Order("created_at desc").Error; err != nil {
		return
	}
	return
}

func (o order) FindById(id uint) (orders *entities.Orders, err error) {
	if err = o.baseRepo.GormDB.Where("id = ?", id).Find(&orders).Debug().Error; err != nil {
		return
	}
	return

}

func (o order) FindRestaurantOrder(resId uint) (order []*entities.Orders, err error) {
	if err = o.baseRepo.GormDB.Where("restaurant_id = ? AND status IN (?,?)", resId, constant.ORDER_PLACED, constant.ORDER_PROCESSING).
		Preload("OrderItems").
		Preload("Restaurant").
		Preload("User").
		Find(&order).Error; err != nil {
		return
	}
	return
}
