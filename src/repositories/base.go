package repositories

import (
	"github.com/thoas/go-funk"
	"gorm.io/gorm"
)

type BaseRepo struct {
	GormDB *gorm.DB
}

type SearchFunction func(db *gorm.DB) *gorm.DB

type FilterInterface interface {
	FilterList(db *gorm.DB) *gorm.DB
}

type RepositoryInterface interface {
	Model(arg interface{}) SearchFunction
	Select(query interface{}, args ...interface{}) SearchFunction
	Joins(query string, args ...interface{}) SearchFunction
	GroupBy(fieldName string) SearchFunction
	Limit(limit int) SearchFunction
	Offset(offset int) SearchFunction
	Distinct(args ...interface{}) SearchFunction
	SortBy(fieldName string, direction string) SearchFunction
	Where(query interface{}, args ...interface{}) SearchFunction
	Or(query interface{}, args ...interface{}) SearchFunction
	Filter(filter FilterInterface) SearchFunction
	Preload(preload string, args ...interface{}) SearchFunction
	Raw(fn SearchFunction) SearchFunction
	Unscoped() SearchFunction
}

func (*BaseRepo) Model(arg interface{}) SearchFunction {
	return func(db *gorm.DB) *gorm.DB {
		return db.Model(arg)
	}
}

func (*BaseRepo) Select(query interface{}, args ...interface{}) SearchFunction {
	return func(db *gorm.DB) *gorm.DB {
		return db.Select(query, args...)
	}
}

func (*BaseRepo) Joins(query string, args ...interface{}) SearchFunction {
	return func(db *gorm.DB) *gorm.DB {
		return db.Joins(query, args...)
	}
}

func (*BaseRepo) GroupBy(fieldName string) SearchFunction {
	return func(db *gorm.DB) *gorm.DB {
		return db.Group(fieldName)
	}
}

func (*BaseRepo) Limit(limit int) SearchFunction {
	return func(db *gorm.DB) *gorm.DB {
		return db.Limit(limit)
	}
}

func (*BaseRepo) Distinct(args ...interface{}) SearchFunction {
	return func(db *gorm.DB) *gorm.DB {
		return db.Distinct(args...)
	}
}

func (*BaseRepo) Offset(offset int) SearchFunction {
	return func(db *gorm.DB) *gorm.DB {
		return db.Offset(offset)
	}
}

func (*BaseRepo) SortBy(fieldName string, direction string) SearchFunction {
	return func(db *gorm.DB) *gorm.DB {
		return db.Order(fieldName + " " + direction)
	}
}

func (*BaseRepo) Where(query interface{}, args ...interface{}) SearchFunction {
	return func(db *gorm.DB) *gorm.DB {
		return db.Where(query, args...)
	}
}

func (*BaseRepo) Or(query interface{}, args ...interface{}) SearchFunction {
	return func(db *gorm.DB) *gorm.DB {
		return db.Or(query, args...)
	}
}

func (*BaseRepo) Filter(filter FilterInterface) SearchFunction {
	return func(db *gorm.DB) *gorm.DB {
		return filter.FilterList(db)
	}
}

func (*BaseRepo) Preload(preload string, args ...interface{}) SearchFunction {
	return func(db *gorm.DB) *gorm.DB {
		return db.Preload(preload, args...)
	}
}

func (*BaseRepo) Unscoped() SearchFunction {
	return func(db *gorm.DB) *gorm.DB {
		return db.Unscoped()
	}
}

func (*BaseRepo) Raw(fn SearchFunction) SearchFunction {
	return fn
}

type SearchBuilder []SearchFunction

func (searchFunctions *SearchBuilder) AsScopes() []func(*gorm.DB) *gorm.DB {
	return funk.Map(*searchFunctions, func(searchFunction SearchFunction) func(*gorm.DB) *gorm.DB {
		return func(db *gorm.DB) *gorm.DB {
			return searchFunction(db)
		}
	}).([]func(*gorm.DB) *gorm.DB)
}
