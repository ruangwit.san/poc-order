package repositories

import "poc-orders/src/entities"

type RestaurantRepositoryInterface interface {
	Find() ([]*entities.Restaurant, error)
}

type restaurant struct {
	baseRepo *BaseRepo
}

func RestaurantNew(repo BaseRepo) RestaurantRepositoryInterface {
	return &restaurant{
		baseRepo: &repo,
	}
}

func (re restaurant) Find() (restaurants []*entities.Restaurant, err error) {
	if err = re.baseRepo.GormDB.Find(&restaurants).Debug().Error; err != nil {
		return
	}
	return
}
