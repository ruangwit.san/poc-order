package entities

import "time"

type Restaurant struct {
	ID          uint    `gorm:"column:id" json:"id"`
	Name        string  `gorm:"column:name" json:"name"`
	Description string  `gorm:"column:description" json:"description"`
	TypeOfFood  string  `gorm:"column:type_of_food" json:"typeOfFood"`
	Meal        []*Meal `gorm:"foreignKey:RestaurantId"`

	CreatedAt time.Time `gorm:"column:created_at"`
	UpdatedAt time.Time `gorm:"column:updated_at"`
	DeletedAt time.Time `gorm:"column:deleted_at"`
}

func NewRestaurant() *Restaurant {
	return &Restaurant{}
}

func (Restaurant) TableName() string {
	return "restaurant"
}

func (r *Restaurant) GetOutput(display string) (newR Restaurant) {
	switch display {
	case "display":
		newR.ID = r.ID
		newR.Name = r.Name
		newR.Description = r.Description
		newR.TypeOfFood = r.TypeOfFood
	default:
		newR = *r

	}
	return

}
