package entities

import (
	"time"
)

type OrderItem struct {
	ID       uint `gorm:"column:id" json:"id"`
	OrderId  uint `gorm:"column:order_id" json:"orderId"`
	Quantity int  `gorm:"column:quantity" json:"quantity"`
	MealId   uint `gorm:"column:meal_id" json:"mealId"`
}

type OrderMealDetail struct {
	MealId   uint `json:"id"`
	Quantity int  `json:"quantity"`
}

type Orders struct {
	ID           uint         `gorm:"column:id" json:"id"`
	Status       string       `gorm:"column:status" json:"status"`
	SubTotal     float32      `gorm:"column:sub_total" json:"subTotal"`
	TotalAmount  float32      `gorm:"column:total_amount" json:"totalAmount"`
	TotalVat     float32      `gorm:"column:total_vat" json:"totalVat"`
	RestaurantId int          `gorm:"column:restaurant_id" json:"restaurantId"`
	PleacedBy    uint         `gorm:"column:placed_by" json:"pleacedBy"`
	User         *User        `gorm:"foreignKey:placed_by" json:"user"`
	Restaurant   *Restaurant  `gorm:"foreignKey:restaurant_id" json:"restaurant"`
	OrderItems   []*OrderItem `gorm:"foreignKey:order_id;references:id" json:"orderItems"`

	CreatedAt *time.Time `gorm:"column:created_at"`
	UpdatedAt *time.Time `gorm:"column:updated_at"`
	DeletedAt *time.Time `gorm:"column:deleted_at"`
}

// Orders creates a new orders
func NewOrders() *Orders {
	return &Orders{}
}

// TableName db
func (Orders) TableName() string {
	return "orders"
}

func (o *Orders) GetOutput(display string) Orders {
	mo := Orders{}
	switch display {
	case "display":
		mo.ID = o.ID
		mo.Status = o.Status
		mo.SubTotal = o.SubTotal
		mo.TotalAmount = o.TotalAmount
		mo.TotalVat = o.TotalVat
		mo.RestaurantId = o.RestaurantId
		mo.OrderItems = o.OrderItems
		mo.PleacedBy = o.PleacedBy
		mo.CreatedAt = o.CreatedAt
		mo.UpdatedAt = o.UpdatedAt
		if o.User != nil {
			mo.User = o.User
		}
		if o.Restaurant != nil {
			mo.Restaurant = o.Restaurant
		}
	default:
		mo = *o
	}

	return mo
}
