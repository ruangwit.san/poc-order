package entities

type User struct {
	Id        string `gorm:"column:id"`
	Firstname string `gorm:"column:first_name"`
	Lastname  string `gorm:"column:last_name"`
	Email     string `gorm:"column:email"`
}

// User creates a new User
func NewUser() *User {
	return &User{}
}

// TableName db
func (User) TableName() string {
	return "user"
}

// GetAPIOutput output data
func (datas *User) GetOutput() (output User) {
	output = *datas
	return
}
