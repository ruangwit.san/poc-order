package entities

import "time"

type MealModel struct {
	ID           uint    `json:"id"`
	RestaurantId uint    `json:"restaurantId"`
	NameTh       string  `json:"nameTh"`
	NameEn       string  `json:"nameEn"`
	Description  string  `json:"description"`
	Price        float32 `json:"price"`
	Quantity     int32   `json:"quantity"`

	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
	DeletedAt time.Time `json:"deletedAt"`
}

type Meal struct {
	ID           uint    `gorm:"column:id"`
	RestaurantId uint    `gorm:"column:restaurant_id"`
	NameTh       string  `gorm:"column:name_th"`
	NameEn       string  `gorm:"column:name_en"`
	Description  string  `gorm:"column:description"`
	Price        float32 `gorm:"column:price"`

	CreatedAt time.Time `gorm:"column:created_at"`
	UpdatedAt time.Time `gorm:"column:updated_at"`
	DeletedAt time.Time `gorm:"column:deleted_at"`
}

func NewMeal() *Meal {
	return &Meal{}
}

func (Meal) TableName() string {
	return "meal"

}

func (m Meal) ToMealModel() *MealModel {

	model := MealModel{
		ID:           m.ID,
		RestaurantId: m.RestaurantId,
		NameEn:       m.NameEn,
		NameTh:       m.NameTh,
		Price:        m.Price,
	}

	return &model
}

func (m MealModel) ToMealModel() *Meal {

	meal := Meal{}

	return &meal
}
