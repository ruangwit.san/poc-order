package routers

import (
	"fmt"
	"poc-orders/src/controllers"
	"poc-orders/src/db"

	customContext "poc-orders/src/core/context"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func Setup() *fiber.App {

	app := fiber.New()
	app.Static("/", "public")

	app.Use(cors.New(cors.ConfigDefault))

	app.Use(func(c *fiber.Ctx) error {
		c.Locals("cc", customContext.New(c))
		return c.Next()
	})

	//Setup database
	database := db.NewConnection()

	//Register Controllers
	c := controllers.NewControllers(database)

	orders := app.Group("/v1/orders")
	orders.Get("/find-all", c.OrderController.FindAll)
	orders.Get("/find-by-id/:id", c.OrderController.FindById)
	orders.Post("/create", c.OrderController.Create)
	orders.Put("/update-status", c.OrderController.UpdateStatus)

	restaurant := app.Group("/v1/restaurant")
	restaurant.Get("/orders/:id", c.OrderController.FindRestaurantOrder)

	fmt.Println("✅ service start ✅")
	return app

}
